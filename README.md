# FormHook

[![pipeline status](https://gitlab.com/mlaopane/formhook/badges/master/pipeline.svg)](https://gitlab.com/mlaopane/formhook/commits/master)
[![coverage report](https://gitlab.com/mlaopane/formhook/badges/master/coverage.svg)](https://gitlab.com/mlaopane/formhook/commits/master)

## Purpose

A library to handle forms in React.

## Installation

```bash
yarn add @mlaopane/formhook
```

OR

```bash
npm i --save @mlaopane/formhook
```

## Examples

### Simple

```jsx
// LoginForm.jsx

import React from 'react';
import { Form, Input, Button } from '@mlaopane/formhook';
// Your custom validator (defined below)
import emailValidator from './emailValidator';

export default function SignInForm() {
    const initialValues = {
        email: '',
        password: '',
    };

    const handleSubmit = ({ form, dispatch }) => {
        /**
         * Do whatever you need with the values
         * like sending them to an API
         */
        console.log(form.values);

        /**
         * Set form.isSubmitting to `false`
         * The provided Button component is automatically disabled
         * when the form is being submitted
         */
        dispatch({ type: 'END_SUBMIT' });
    };

    // The state of the form is handled by the Form component
    return (
        <Form initialValues={initialValues} onSubmit={handleSubmit}>
            <Input name='email' validate={emailValidator.validate} />
            <Input name='password' />
            <Button>Sign in</Button>
        </Form>
    );
}
```

```javascript
// emailValidator.js

/**
 * You need to return an empty string if there is no error
 */
export const validate = async ({ field }) => {
    if (!field.value) {
        return 'E-mail is mandatory';
    }
    if (!field.value.includes('@')) {
        return 'Invalid e-mail';
    }
    return '';
};
```
