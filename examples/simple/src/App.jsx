import React from 'react';
import './App.css';
import NicknameForm from './components/NicknameForm';
import SignupForm from './components/SignupForm';

const App = () => {
    return (
        <div id='app'>
            <NicknameForm />
            {/* <SignupForm /> */}
        </div>
    );
};
export default App;
