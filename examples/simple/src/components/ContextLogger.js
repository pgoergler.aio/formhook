import React from 'react';
import { Spy } from '@mlaopane/formhook';

export default function ContextLogger() {
    const [formState, setFormState] = React.useState({});

    function hookContext({ form }) {
        setFormState(form);
    }

    return (
        <>
            <Spy hookContext={hookContext} />
            <div
                style={{
                    backgroundColor: 'hsl(212, 20%, 25%)',
                    color: 'hsl(212, 60%, 97%)',
                    marginTop: '16px',
                    padding: '16px',
                }}
            >
                <pre>{JSON.stringify(formState, null, 4)}</pre>
            </div>
        </>
    );
}
