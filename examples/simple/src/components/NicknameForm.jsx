import React from 'react';
import {
    Button,
    Form,
    Input,
    Spy,
    Label,
    ErrorMessage,
} from '@mlaopane/formhook';
import { validate } from '../validators/nickname.validator';
import ContextLogger from './ContextLogger';

export default function NicknameForm() {
    const [formState, setFormState] = React.useState({});

    function hookContext({ form }) {
        setFormState(form);
    }

    function handleSubmit({ form, dispatch }) {
        setTimeout(() => {
            dispatch({ type: 'END_SUBMIT' });
        }, 2000);
    }

    return (
        <Form initialValues={{ nickname: '' }} onSubmit={handleSubmit}>
            <Label>Nickname</Label>
            <Input name='nickname' validate={validate} />
            <ErrorMessage name='nickname' />
            <Button>Submit</Button>
            {formState.isSubmitting && (
                <div style={{ textAlign: 'center' }}>Submit in progress...</div>
            )}
            <Spy hookContext={hookContext} />
            <ContextLogger />
        </Form>
    );
}
