import React from 'react';

import {
    Field,
    Form,
    Input,
    Label,
    CheckboxGroup,
    RadioButtonGroup,
    Button,
    Email,
    Password,
    ErrorMessage,
} from '@mlaopane/formhook';
import {
    emailValidator,
    passwordValidator,
    passwordConfirmationValidator,
} from '../validators';

const handleSubmit = ({ form, dispatch }) => {
    const { values } = form;
    console.log('signup --> success', { values });
    dispatch({ type: 'END_SUBMIT' });
};

const SignupForm = (props) => {
    const initialValues = {
        gender: '',
        givenName: '',
        familyName: '',
        email: '',
        password: '',
        passwordConfirmation: '',
        conditions: false,
        newsletter: false,
    };

    return (
        <Form initialValues={initialValues} onSubmit={handleSubmit} {...props}>
            {/* Gender */}
            <Field>
                <div style={{ display: 'flex' }}>
                    <RadioButtonGroup
                        name='gender'
                        id='female'
                        label='Female'
                    />
                    <RadioButtonGroup name='gender' id='male' label='Male' />
                </div>
            </Field>

            {/* Given name */}
            <Field>
                <Label>Given name</Label>
                <Input
                    id='signup-given-name'
                    name='givenName'
                    validate={async ({ field }) =>
                        field.value ? '' : 'The given name is required'
                    }
                    autoComplete='given-name'
                    type='text'
                />
                <ErrorMessage name='givenName' />
            </Field>

            {/* Family name */}
            <Field>
                <Label>Family name</Label>
                <Input
                    id='signup-family-name'
                    name='familyName'
                    validate={async ({ field }) =>
                        field.value ? '' : 'The family name is required'
                    }
                    autoComplete='family-name'
                />
                <ErrorMessage name='familyName' />
            </Field>

            {/* E-mail */}
            <Field>
                <Label>E-mail</Label>
                <Email id='signup-email' {...emailValidator} />
                <ErrorMessage name='email' />
            </Field>

            {/* Password */}
            <Field>
                <Label>Password</Label>
                <Password id='signup-password' {...passwordValidator} />
                <ErrorMessage name='password' />
            </Field>

            {/* Password confirmation */}
            <Field>
                <Label>Password confirmation</Label>
                <Password
                    id='signup-password-confirmation'
                    name={'passwordConfirmation'}
                    validate={passwordConfirmationValidator.validate(
                        'password',
                    )}
                />
                <ErrorMessage name='passwordConfirmation' />
            </Field>

            {/* Spam */}
            <Field>
                <CheckboxGroup
                    id='signup-newsletter'
                    name={'newsletter'}
                    label='I like spam'
                />
            </Field>

            {/* Conditions */}
            <Field>
                <CheckboxGroup
                    id='signup-conditions'
                    name={'conditions'}
                    label='I agree'
                    validate={async ({ field }) => {
                        return !field.value ? 'You must agree' : '';
                    }}
                />
                <ErrorMessage name='conditions' />
            </Field>

            {/* Submit */}
            <Button
                type='submit'
                action='submit'
                disabled={(form) => form.isSubmitting}
            >
                Sign up
            </Button>
        </Form>
    );
};

export default SignupForm;
