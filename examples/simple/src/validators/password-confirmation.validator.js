export function validate(passwordFieldName) {
    return async function validate({ field, form }) {
        if (!field.value) {
            return 'The password confirmation is mandatory';
        }
        if (field.value !== form.values[passwordFieldName]) {
            return 'Passwords must match';
        }
        return '';
    };
}

export default {
    validate,
    validateOnBlur: validate,
};
