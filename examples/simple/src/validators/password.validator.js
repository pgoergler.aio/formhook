export const validate = async ({ field }) => {
    if (!field.value) {
        return 'Password is mandatory';
    }
    return '';
};

export default {
    validate,
};
