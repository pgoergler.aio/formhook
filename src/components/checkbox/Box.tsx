import React from 'react';
import './Box.scss';

export type BoxProps = {
    checked: boolean;
    onClick: React.MouseEventHandler;
};

const Box = ({ checked = false, onClick, ...props }: BoxProps) => {
    return (
        <div
            className={`form-box${checked ? ' checked' : ''}`}
            onClick={onClick}
            {...props}
        />
    );
};

export default Box;
