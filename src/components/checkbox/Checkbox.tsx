import React from 'react';
import { useInput, useFieldTrigger } from '../../hooks';
import Box from './Box';
import './Checkbox.scss';

type CheckboxProps = {
    name: string;
    id?: string;
    label?: string;
    autoComplete?: string;
    component?: any;
    validate?: any;
    validateOnBlur?: any;
    validateOnChange?: any;
};

const Checkbox = ({
    name,
    id = name,
    label = '',
    autoComplete = 'off',
    component: Component = undefined,
    validate = undefined,
    validateOnChange = undefined,
    validateOnBlur = undefined,
    ...props
}: CheckboxProps) => {
    const [value, handleChange, handleFocus, handleBlur] = useInput({
        name,
        validate,
        validateOnChange,
        validateOnBlur,
    });

    const [checkboxRef, toggleCheckbox] = useFieldTrigger({
        name,
        value: !value,
    });

    const BoxComponent = Component ? Component : Box;

    return (
        <div className='form-checkbox' {...props}>
            <input
                ref={checkboxRef}
                className='form-input'
                type='checkbox'
                id={id}
                name={name}
                checked={value}
                onChange={handleChange}
                onFocus={handleFocus}
                onBlur={handleBlur}
                {...props}
            />
            <BoxComponent checked={value} onClick={toggleCheckbox} />
        </div>
    );
};

export default Checkbox;
