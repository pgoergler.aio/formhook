import React from 'react';
import Checkbox from './Checkbox';
import CheckboxLabel from './CheckboxLabel';
import './CheckboxGroup.scss';

type CheckboxGroupProps = {
    name: string;
    value: any;
    label?: string;
    id?: string;
    [key: string]: any;
};

const CheckboxGroup = ({
    name,
    value,
    label = '',
    id = name,
    ...props
}: CheckboxGroupProps) => {
    return (
        <div className='form-checkbox-group'>
            <Checkbox name={name} label={label} id={id} {...props} />
            <CheckboxLabel id={id} name={name}>
                {label}
            </CheckboxLabel>
        </div>
    );
};

export default CheckboxGroup;
