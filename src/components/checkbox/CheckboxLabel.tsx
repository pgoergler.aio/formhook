import React from 'react';
import './CheckboxLabel.scss';
import { useCheckbox } from '../../hooks';

type CheckboxLabelProps = {
    name: string;
    id: string;
};

const CheckboxLabel = ({
    children,
    id,
    name,
    ...props
}: React.PropsWithChildren<CheckboxLabelProps>) => {
    const [toggleCheckbox] = useCheckbox();

    const onClick = (event) => {
        event.preventDefault();
        toggleCheckbox({ name });
    };

    return (
        <label
            className='form-checkbox-label'
            onClick={onClick}
            htmlFor={id}
            {...props}
        >
            {children}
        </label>
    );
};

export default CheckboxLabel;
