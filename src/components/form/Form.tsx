import React from 'react';
import { FormContext } from '../../context';
import { useForm, useFormValidator } from '../../hooks';
import { FormDispatch, FormState } from '../../types';
import './Form.scss';

type FormProps = {
    initialValues: FormState['values'];
    onSubmit: ({
        form,
        dispatch,
    }: {
        form: FormState;
        dispatch: FormDispatch;
    }) => undefined;
    component?: any;
};

function formIsValid(errors) {
    const fieldNames = Object.keys(errors);

    function formHasNoError(result, fieldName) {
        return result && !errors[fieldName];
    }

    return fieldNames.reduce(formHasNoError, true);
}

const Form = ({
    children,
    initialValues,
    onSubmit = () => undefined,
    component: CustomForm = null,
    ...props
}: React.PropsWithChildren<FormProps>) => {
    const [form, dispatch] = useForm({ initialValues });
    const [validateForm] = useFormValidator({ form, dispatch });

    const handleSubmit = async (event: React.FormEvent) => {
        event.preventDefault();
        dispatch({ type: 'START_SUBMIT' });

        const errors = await validateForm();

        if (!formIsValid(errors)) {
            dispatch({ type: 'END_SUBMIT' });
            return;
        }

        onSubmit({ form, dispatch });
    };

    if (CustomForm) {
        return (
            <FormContext.Provider value={[form, dispatch]}>
                <CustomForm noValidate onSubmit={handleSubmit} {...props}>
                    {children}
                </CustomForm>
            </FormContext.Provider>
        );
    }
    return (
        <FormContext.Provider value={[form, dispatch]}>
            <form
                noValidate
                onSubmit={handleSubmit}
                className='form'
                {...props}
            >
                {children}
            </form>
        </FormContext.Provider>
    );
};

export default Form;
