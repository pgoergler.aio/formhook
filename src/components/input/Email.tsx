import React from 'react';
import Input from './Input';

type EmailProps = {
    name: string;
    id: string;
    autoComplete: string;
};

const Email = ({
    name = 'email',
    id = name,
    autoComplete = 'email',
    ...props
}: EmailProps) => {
    return (
        <Input
            type='email'
            id={id}
            name={name}
            autoComplete={autoComplete}
            {...props}
        />
    );
};

export default Email;
