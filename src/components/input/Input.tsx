import React from 'react';
import { useInput } from '../../hooks';
import { AsyncValidationFunction } from '../../types';
import './Input.scss';

type InputProps = {
    autoComplete: string;
    component?: any;
    id: string;
    name: string;
    type: string;
    validate?: AsyncValidationFunction;
    validateOnChange?: AsyncValidationFunction;
    validateOnBlur?: AsyncValidationFunction;
};

const Input = ({
    name,
    id = name,
    validate = undefined,
    validateOnChange = undefined,
    validateOnBlur = undefined,
    type = 'text',
    component = null,
    autoComplete = 'off',
    ...props
}: InputProps) => {
    const [value, handleChange, handleFocus, handleBlur] = useInput({
        name,
        validate,
        validateOnChange,
        validateOnBlur,
    });

    const Component = component || 'input';

    return (
        <Component
            className='form-input'
            id={id}
            name={name}
            type={type}
            value={value}
            onChange={handleChange}
            onFocus={handleFocus}
            onBlur={handleBlur}
            {...props}
        />
    );
};

export default Input;
