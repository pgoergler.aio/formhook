import React from 'react';
import Input from './Input';

type PasswordProps = {
    name: string;
    id: string;
    autoComplete: string;
};

const Password = ({
    name = 'password',
    id = name,
    autoComplete = 'password',
    ...props
}: PasswordProps) => {
    return (
        <Input
            type='password'
            id={id}
            name={name}
            autoComplete={autoComplete}
            {...props}
        />
    );
};

export default Password;
