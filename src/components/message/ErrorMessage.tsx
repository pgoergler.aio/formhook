import React from 'react';
import { useError } from '../../hooks';
import './ErrorMessage.scss';

type ErrorMessageProps = {
    name: string;
    component?: React.FunctionComponent<any>;
};

const ErrorMessage = ({
    name,
    component: CustomerErrorMessage,
    ...props
}: ErrorMessageProps) => {
    const [error, focused] = useError(name);
    const status = focused !== name ? 'default' : 'silent';

    if (CustomerErrorMessage) {
        return <CustomerErrorMessage {...props}>{error}</CustomerErrorMessage>;
    }

    return (
        <div className={`error-message ${status}`} {...props}>
            {error}
        </div>
    );
};

export default ErrorMessage;
