import React from 'react';
import './Radio.scss';

type RadioProps = { checked: boolean; onClick: React.MouseEventHandler };

const Radio = ({ checked = false, onClick, ...props }: RadioProps) => {
    return (
        <div
            className={`form-radio${checked ? ' checked' : ''}`}
            onClick={onClick}
            {...props}
        />
    );
};

export default Radio;
