import React from 'react';
import { useInput, useRadio } from '../../hooks';
import { AsyncValidationFunction } from '../../types';
import Radio from './Radio';
import './RadioButton.scss';

type RadioButtonProps = {
    name: string;
    id: string;
    label: string;
    autoComplete?: string;
    component?: React.FunctionComponent<any>;
    validate?: AsyncValidationFunction;
    validateOnChange?: AsyncValidationFunction;
    validateOnBlur?: AsyncValidationFunction;
};

const RadioButton = ({
    name,
    id = name,
    label = '',
    autoComplete = 'off',
    component: CustomRadio = undefined,
    validate = undefined,
    validateOnChange = undefined,
    validateOnBlur = undefined,
    ...props
}: RadioButtonProps) => {
    const [value, handleChange, handleFocus, handleBlur] = useInput({
        name,
        validate,
        validateOnChange,
        validateOnBlur,
    });
    const [selectRadio] = useRadio({ name, id });

    const RadioComponent = CustomRadio ? CustomRadio : Radio;

    return (
        <div className='form-radio-button' {...props}>
            <input
                className='form-input'
                type='radio'
                name={name}
                id={id}
                value={id}
                checked={value === id}
                onChange={handleChange}
                onFocus={handleFocus}
                onBlur={handleBlur}
                {...props}
            />
            <RadioComponent checked={value === id} onClick={selectRadio} />
        </div>
    );
};

export default RadioButton;
