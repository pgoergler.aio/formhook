import React from 'react';
import RadioButton from './RadioButton';
import RadioButtonLabel from './RadioButtonLabel';
import './RadioButtonGroup.scss';

type RadioButtonGroupProps = {
    name: string;
    id: string;
    value: any;
    label: string;
};

const RadioButtonGroup = ({
    name,
    value,
    label = '',
    id = name,
    ...props
}: RadioButtonGroupProps) => {
    return (
        <div className='form-radio-group'>
            <RadioButton name={name} label={label} id={id} {...props} />
            <RadioButtonLabel id={id} name={name}>
                {label}
            </RadioButtonLabel>
        </div>
    );
};

export default RadioButtonGroup;
