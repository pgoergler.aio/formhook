import React from 'react';
import './RadioButtonLabel.scss';
import { useField } from '../../hooks';

type RadioButtonLabelProps = { id: string; name: string };

const RadioButtonLabel = ({
    children,
    id,
    name,
    ...props
}: React.PropsWithChildren<RadioButtonLabelProps>) => {
    const [changeField] = useField();

    const onClick = (event) => {
        event.preventDefault();
        changeField({ name, value: id });
    };

    return (
        <label
            className='form-radio-label'
            onClick={onClick}
            htmlFor={id}
            {...props}
        >
            {children}
        </label>
    );
};

export default RadioButtonLabel;
