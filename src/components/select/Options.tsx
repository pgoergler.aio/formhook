import React from 'react';

type Option = {
    label: string;
    value: any;
};

const Options = ({
    options = [],
    defaultOption = { value: '', label: 'Select an option' },
}: {
    options: Option[];
    defaultOption?: Option;
}) => {
    let optionsList = options.map(({ value, label }) => (
        <option key={value} id={value} value={value}>
            {label}
        </option>
    ));

    const { value, label } = defaultOption;

    optionsList.unshift(
        <option key='default' id={value} value={value}>
            {label}
        </option>,
    );

    return <>{optionsList}</>;
};

export default Options;
