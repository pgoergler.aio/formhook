import React from 'react';
import { useSelect } from '../../hooks';
import Options from './Options';
import './Select.scss';
import { Option } from '../../types';

type SelectorProps = {
    name: string;
    multiple: boolean;
    options: Option[];
    defaultLabel: string;
};

type SelectProps = {
    name: string;
    id?: string;
    defaultLabel: string;
    multiple: boolean;
    getInitialOptions: () => Promise<Option[]>;
    validate: any;
    validateOnChange: any;
    validateOnBlur: any;
    component: React.JSXElementConstructor<SelectorProps>;
    [key: string]: any;
};

const Select = ({
    name,
    id = name,
    defaultLabel = 'Select an option...',
    multiple = false,
    getInitialOptions = () => Promise.resolve([]),
    validate = undefined,
    validateOnChange = undefined,
    validateOnBlur = undefined,
    component: Selector,
    ...props
}: SelectProps) => {
    const [options, value, handleChange, handleFocus, handleBlur] = useSelect({
        name,
        getInitialOptions,
        validate,
        validateOnChange,
        validateOnBlur,
    });

    return (
        <React.Fragment>
            <select
                className={`form-select${Selector ? ' hidden' : ''}`}
                id={id}
                name={name}
                value={value}
                multiple={multiple}
                onChange={handleChange}
                onFocus={handleFocus}
                onBlur={handleBlur}
                {...props}
            >
                <Options options={options} />
            </select>
            {Selector && (
                <Selector
                    name={name}
                    multiple={multiple}
                    options={options}
                    defaultLabel={defaultLabel}
                />
            )}
        </React.Fragment>
    );
};

export default Select;
