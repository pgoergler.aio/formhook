import React from 'react';
import { useOption } from '../../../hooks';
import { Option } from '../../../types';
import './SelectorOption.scss';

type SelectorOptionProps = {
    name: string;
    onSelect: React.MouseEventHandler | undefined;
    multiple: boolean;
    selected: boolean;
    option: Option;
    [key: string]: any;
};

const SelectorOption = ({
    name,
    onSelect = undefined, // Additional behavior on select
    multiple = false,
    selected = false,
    option = { label: 'Select an option', value: '' },
    ...props
}: SelectorOptionProps) => {
    const [handleClick] = useOption({ name, option, multiple, onSelect });

    return (
        <div
            className={`form-selector-option${selected ? ' selected' : ''}`}
            onClick={handleClick}
            {...props}
        >
            {option.label}
        </div>
    );
};

export default SelectorOption;
