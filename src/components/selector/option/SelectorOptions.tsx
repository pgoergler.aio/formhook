import React from 'react';
import { FormContext } from '../../../context';
import { Option } from '../../../types';
import SelectorOption from './SelectorOption';

type SelectorOptionsProps = {
    name: string;
    multiple: boolean;
    onSelect: React.MouseEventHandler | undefined;
    options: Option[];
    [key: string]: any;
};

const SelectorOptions = ({
    name,
    onSelect,
    options,
    multiple = false,
    ...props
}: SelectorOptionsProps) => {
    const [form] = React.useContext(FormContext);

    return (
        <>
            {options.map((option) => {
                let selected;

                if (!multiple) {
                    selected = option.value === form.values[name];
                } else {
                    selected = form.values[name].includes(option.value);
                }

                return (
                    <SelectorOption
                        name={name}
                        option={option}
                        multiple={multiple}
                        selected={selected}
                        onSelect={onSelect}
                        key={option.value}
                        {...props}
                    />
                );
            })}
        </>
    );
};

export default SelectorOptions;
