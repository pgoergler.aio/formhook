import React from 'react';
import { Option } from '../../../types';

type MultipleSelectionProps = {
    selectedOptions: Option[];
    defaultLabel: string;
};

const MultipleSelection = ({
    selectedOptions = [],
    defaultLabel = 'Select an option...',
}: MultipleSelectionProps) => {
    if (0 === selectedOptions.length) {
        return <>{defaultLabel}</>;
    }

    return <>{`${selectedOptions.length} selected`}</>;
};

export default MultipleSelection;
