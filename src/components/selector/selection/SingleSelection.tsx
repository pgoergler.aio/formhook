import React from 'react';
import { Option } from '../../../types';

type SingleSelectionProps = {
    selectedOption: Option;
    defaultLabel: string;
};

const SingleSelection = ({
    selectedOption,
    defaultLabel = 'Select an option...',
}: SingleSelectionProps) => {
    return <>{selectedOption.label || defaultLabel}</>;
};

export default SingleSelection;
