import React from 'react';

import './Card.scss';

const Card = ({ children, ...props }: React.PropsWithChildren<any>) => (
    <div {...props} className='card'>
        {children}
    </div>
);

export default Card;
