import React from 'react';
import './Field.scss';

const Field = ({ children, ...props }: React.PropsWithChildren<any>) => {
    return (
        <div className='form-field' {...props}>
            {children}
        </div>
    );
};

export default Field;
