import React from 'react';
import './FormTitle.scss';

const FormTitle = ({ children, ...props }: React.PropsWithChildren<any>) => {
    return (
        <h3 {...props} className='form-title'>
            {children}
        </h3>
    );
};

export default FormTitle;
