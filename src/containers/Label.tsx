import React from 'react';
import './Label.scss';

const Label = ({ children, ...props }: React.PropsWithChildren<any>) => (
    <div {...props} className='form-label'>
        {children}
    </div>
);

export default Label;
