import React from 'react';
import { FormState } from '../types';

type FormContextType = [FormState, React.Dispatch<any>];

const initialState: FormState = {
    values: {},
    errors: {},
    touched: {},
    validators: {},
    options: {},
    focused: '',
    isSubmitting: false,
    isValid: false,
};

const dispatch: React.Dispatch<any> = () => {};

const FormContext = React.createContext<FormContextType>([
    initialState,
    dispatch,
]);

export default FormContext;
