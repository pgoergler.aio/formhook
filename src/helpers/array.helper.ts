/**
 * Get a shallow copy of an array, splice at the provided index
 */
export function splice(arrayToSplice: any[], index: number) {
    if (index < 0) {
        throw new Error('The index parameter must be a positive integer');
    }

    return [
        ...arrayToSplice.slice(0, index),
        ...arrayToSplice.slice(index + 1, arrayToSplice.length),
    ];
}
