export function formIsValid(form) {
    return Object.values(form.errors).join('').length === 0;
}
