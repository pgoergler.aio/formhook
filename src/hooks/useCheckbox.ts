import useField from './useField';
import React from 'react';
import { FormContext } from '../context';

export default function useCheckbox() {
    const [form] = React.useContext(FormContext);
    const [changeField] = useField();

    const toggleCheckbox = ({ name }) => {
        changeField({ name, value: !form.values[name] });
    };

    return [toggleCheckbox];
}
