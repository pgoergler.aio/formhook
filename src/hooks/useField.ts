import React from 'react';
import { FormContext } from '../context';

export default function useField() {
    const [, dispatch] = React.useContext(FormContext);

    const changeField = ({ name, value }) => {
        const field = { name, value };
        dispatch({ type: 'CHANGE_FIELD', payload: { field } });
    };

    const focusField = (name) => {
        const field = { name };
        dispatch({ type: 'FOCUS_FIELD', payload: { field } });
    };

    const blurField = (name) => {
        const field = { name };
        dispatch({ type: 'BLUR_FIELD', payload: { field } });
    };

    return [changeField, focusField, blurField];
}
