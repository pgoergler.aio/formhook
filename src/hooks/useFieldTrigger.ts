import React from 'react';
import useField from './useField';

export type UseFieldTriggerProps = {
    name: string;
    value: any;
};

const useFieldTrigger = ({ name, value }: UseFieldTriggerProps) => {
    const fieldRef = React.useRef('') as any;
    const [changeField] = useField();

    const click = () => {
        if (fieldRef && fieldRef.current && fieldRef.current.click) {
            fieldRef.current.click();
            changeField({ name, value });
        }
    };

    return [fieldRef, click];
};

export default useFieldTrigger;
