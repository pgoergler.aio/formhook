import React from 'react';
import { formReducer } from '../reducers';
import { FormState } from '../types';

function toInitialFieldValue(value: any) {
    return function(stack: { [key: string]: any }, field: string) {
        return {
            ...stack,
            [field]: value,
        };
    };
}

function mapInitialState(fields: string[], initialValue: any) {
    return fields.reduce(toInitialFieldValue(initialValue), {} as FormState);
}

function useForm({ initialValues }: { initialValues: { [key: string]: any } }) {
    const fields = Object.keys(initialValues);
    const errors = mapInitialState(fields, '');
    const validators = mapInitialState(fields, {});
    const touched = mapInitialState(fields, false);
    const options = mapInitialState(fields, []);
    const isValid = Object.values(errors).join('').length === 0;

    const initialState = {
        errors,
        focused: '',
        isValid,
        isSubmitting: false,
        options,
        touched,
        validators,
        values: initialValues,
    };

    return React.useReducer(formReducer, initialState);
}

export default useForm;
