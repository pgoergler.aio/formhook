import React from 'react';
import { FormContext } from '../context';
import useValidator from './useValidator';

type UseInputArgs = {
    name: string;
    validate: any;
    validateOnChange: any;
    validateOnBlur: any;
};

function useInput({
    name,
    validate,
    validateOnChange,
    validateOnBlur,
}: UseInputArgs) {
    const [form, dispatch] = React.useContext(FormContext) as any;
    const [processValidation] = useValidator();
    const value = form.values[name];

    React.useEffect(() => {
        dispatch({
            type: 'SET_FIELD_VALIDATORS',
            payload: {
                field: {
                    name,
                },
                validate,
                validateOnChange,
                validateOnBlur,
            },
        });
    }, [dispatch, name, validate, validateOnBlur, validateOnChange]);

    const handleChange = async ({ target: field }) => {
        dispatch({ type: 'CHANGE_FIELD', payload: { field } });

        const validationFn = validateOnChange || validate;

        if (validationFn) {
            processValidation({ field, validationFn });
        }
    };

    const handleFocus = async ({ target }) => {
        const { name } = target;

        dispatch({
            type: 'FOCUS_FIELD',
            payload: {
                field: { name },
            },
        });
    };

    const handleBlur = async ({ target: field }) => {
        dispatch({ type: 'BLUR_FIELD', payload: { field } });

        const validationFn = validateOnBlur || validate;

        if (validationFn) {
            processValidation({ field, validationFn });
        }
    };

    const validateField = ({ field }) => {
        return processValidation({ field, validationFn: validate });
    };

    return [value, handleChange, handleFocus, handleBlur, validateField];
}

export default useInput;
