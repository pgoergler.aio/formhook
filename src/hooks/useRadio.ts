import React from 'react';
import { FormContext } from '../context';

export default function useRadio({ name, id }) {
    const [, dispatch] = React.useContext(FormContext);

    const selectRadio = (event) => {
        event.preventDefault();
        const field = { name, value: id };
        dispatch({ type: 'CHANGE_FIELD', payload: { field } });
    };

    return [selectRadio];
}
