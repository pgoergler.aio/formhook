import React from 'react';
import { FormContext } from '../context';
import useInput from './useInput';
import { AsyncValidationFunction, Option } from '../types';

type UseSelectArgs = {
    name: string;
    getInitialOptions: () => Promise<Option[]>;
    validate: AsyncValidationFunction | undefined;
    validateOnChange: AsyncValidationFunction | undefined;
    validateOnBlur: AsyncValidationFunction | undefined;
};

export default function useSelect({
    name,
    getInitialOptions,
    validate,
    validateOnChange,
    validateOnBlur,
}: UseSelectArgs) {
    const [form, dispatch] = React.useContext(FormContext);
    const [value, handleChange, handleFocus, handleBlur] = useInput({
        name,
        validate,
        validateOnChange,
        validateOnBlur,
    });
    const options = form.options[name];

    React.useEffect(() => {
        getInitialOptions().then((options) => {
            const field = { name };
            dispatch({
                type: 'SET_FIELD_OPTIONS',
                payload: { field, options },
            });
        });
    }, [name, dispatch, getInitialOptions]);

    return [options, value, handleChange, handleFocus, handleBlur];
}
