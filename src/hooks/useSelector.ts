import React from 'react';
import { FormContext } from '../context';

type UseSelectorArgs = {
    name: string;
    multiple: boolean;
};

export default function useSelector({
    name,
    multiple = false,
}: UseSelectorArgs) {
    const [form] = React.useContext(FormContext);
    const [isOpen, setIsOpen] = React.useState(false);
    const wrapperRef = React.useRef<HTMLDivElement>(null);

    const selectOption = () => {
        if (!multiple) {
            setIsOpen(!isOpen);
        }
    };

    const toggleSelect = () => {
        setIsOpen(!isOpen);
    };

    const handleClickOutside = (event: MouseEvent) => {
        if (
            wrapperRef &&
            wrapperRef.current &&
            null !== event.target &&
            !wrapperRef.current.contains(event.target as Node)
        ) {
            setIsOpen(false);
        }
    };

    React.useEffect(() => {
        document.addEventListener('mousedown', handleClickOutside);
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, []);

    if (!multiple) {
        const selection = form.options[name].find(
            (option) => option.value === form.values[name],
        );
        return { selection, isOpen, wrapperRef, selectOption, toggleSelect };
    }

    const selection = form.options[name].filter((option) => {
        return form.values[name].includes(option.value);
    });

    return { selection, isOpen, wrapperRef, selectOption, toggleSelect };
}
