import React from 'react';
import { FormContext } from '../context';
import { Field, FormState } from '../types';

type ValidationFunctionArgs = {
    field: Field;
    form: FormState;
};

export type AsyncValidationFunction = ({
    field,
    form,
}: ValidationFunctionArgs) => Promise<string>;

function useValidator() {
    const [form, dispatch] = React.useContext(FormContext) as any;

    const processValidation = async ({
        field,
        validationFn,
    }: {
        field: Field;
        validationFn: AsyncValidationFunction;
    }) => {
        const error = await validationFn({ field, form });
        const { name } = field;

        if (error) {
            dispatch({
                type: 'SET_FIELD_ERROR',
                payload: {
                    field: { name },
                    error,
                },
            });
        } else {
            dispatch({
                type: 'CLEAR_FIELD_ERROR',
                payload: {
                    field: { name },
                },
            });
        }
    };

    return [processValidation];
}

export default useValidator;
