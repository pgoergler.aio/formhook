import { Document, Field } from '../types';

type State = {
    errors: Document<string>;
};

type Payload = {
    field: Field;
    error: string;
};

type Action = {
    type: string;
    payload: Payload;
};

type ReducerArgs = {
    state: State;
    action: Action;
};

type ActionArgs = {
    state: State;
    payload: Payload;
};

export function setFieldError({ state, payload }: ActionArgs): State {
    const { field, error } = payload;

    return {
        ...state,
        errors: {
            ...state.errors,
            [field.name]: error,
        },
    };
}

export function clearFieldError({ state, payload }: ActionArgs): State {
    const { field } = payload;

    return setFieldError({
        state,
        payload: {
            field,
            error: '',
        },
    });
}
