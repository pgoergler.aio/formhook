import { Document, Field } from '../types';

type State = {
    values: Document;
    focused: string;
    touched: Document<boolean>;
};

type Payload = {
    field: Field;
};

type ActionArgs = {
    state: State;
    payload: Payload;
};

export function changeField({ state, payload }: ActionArgs) {
    return {
        ...state,
        values: {
            ...state.values,
            [payload.field.name]: payload.field.value,
        },
    };
}

export function focusField({ state, payload }: ActionArgs) {
    return {
        ...state,
        focused: payload.field.name,
    };
}

export function blurField({ state, payload }: ActionArgs) {
    return {
        ...state,
        focused: '',
        touched: {
            ...state.touched,
            [payload.field.name]: true,
        },
    };
}
