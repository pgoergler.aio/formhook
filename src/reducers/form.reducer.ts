import { ActionType } from '../types/Action';
import { Action, FormState } from '../types';
import { startSubmit, endSubmit } from './form-status.reducer';
import { changeField, focusField, blurField } from './field.reducer';
import { setFieldValidators } from './validators.reducer';
import { setFieldOptions } from './options.reducer';
import { setFieldError, clearFieldError } from './error.reducer';

const REDUCERS = {
    START_SUBMIT: startSubmit,
    END_SUBMIT: endSubmit,
    CHANGE_FIELD: changeField,
    FOCUS_FIELD: focusField,
    BLUR_FIELD: blurField,
    SET_FIELD_VALIDATORS: setFieldValidators,
    SET_FIELD_OPTIONS: setFieldOptions,
    SET_FIELD_ERROR: setFieldError,
    CLEAR_FIELD_ERROR: clearFieldError,
};

function getReducerByActionType(type: ActionType) {
    return REDUCERS[type];
}

export default function formReducer(state: FormState, action: Action) {
    const { type, payload } = action;
    const reducer = getReducerByActionType(type);

    return { ...state, ...reducer({ state, payload }) };
}
