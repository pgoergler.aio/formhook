import { Document, Field, Option } from '../types';

type State = {
    options: Document;
};

type Payload = {
    field: Field;
    options: [Option];
};

type ActionArgs = {
    state: State;
    payload: Payload;
};

export function setFieldOptions({ state, payload }: ActionArgs) {
    const { field, options } = payload;

    return {
        ...state,
        options: {
            ...state.options,
            [field.name]: options,
        },
    };
}
