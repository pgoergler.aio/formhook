export type Document<T = any> = { [key: string]: T };
