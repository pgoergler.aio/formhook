type Field = {
    name: string;
    value?: any;
};

export default Field;
