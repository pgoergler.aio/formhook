import { Option, Document } from '../types';

type FormState = {
    values: Document<any>;
    errors: Document<string>;
    touched: Document<boolean>;
    validators: Document<any>;
    options: Document<[Option]>;
    focused: string;
    isSubmitting: boolean;
    isValid: boolean;
};

export default FormState;
