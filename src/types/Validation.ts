import Field from './Field';
import FormState from './FormState';

type ValidationFunctionArgs = {
    field: Field;
    form: FormState;
};

type AsyncValidationFunction = ({
    field,
    form,
}: ValidationFunctionArgs) => Promise<string>;

export default AsyncValidationFunction;
