import { FormState, FormDispatch } from '../types';

const validateForm = ({
    form,
    dispatch,
}: {
    form: FormState;
    dispatch: FormDispatch;
}) => {
    const { values, validators } = form;
    const fieldNames = Object.keys(validators);

    const promises = fieldNames.map(async (name) => {
        const validate = validators[name] && validators[name].validate;
        const value = values[name];

        if (validate) {
            const error = await validate({
                field: { name, value },
                form,
            });
            if (error) {
                dispatch({
                    type: 'SET_FIELD_ERROR',
                    payload: {
                        field: { name },
                        error,
                    },
                });
                return { [name]: error };
            }
        }

        dispatch({
            type: 'CLEAR_FIELD_ERROR',
            payload: {
                field: { name },
            },
        });

        return { [name]: '' };
    });

    return Promise.all(promises);
};

export default { validateForm };
