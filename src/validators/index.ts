import formValidator from './formValidator';
import emailValidator from './emailValidator';
import passwordValidator from './passwordValidator';
import passwordConfirmationVaidator from './passwordConfirmationValidator';

export {
    formValidator,
    emailValidator,
    passwordValidator,
    passwordConfirmationVaidator,
};
