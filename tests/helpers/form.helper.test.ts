import { formIsValid } from 'helpers/form.helper';

test('A form without error should be valid', function() {
    expect(formIsValid({ errors: [] })).toBe(true);
    expect(formIsValid({ errors: [''] })).toBe(true);
});

test('A form with errors should not be valid', function() {
    expect(formIsValid({ errors: ['Error 1A'] })).toBe(false);
    expect(formIsValid({ errors: ['Error 2A', 'Error 2B'] })).toBe(false);
});
