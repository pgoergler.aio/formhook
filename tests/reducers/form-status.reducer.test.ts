import { FormState } from 'types';
import { formReducer } from 'reducers';
import { START_SUBMIT, END_SUBMIT } from 'reducers/actions';
import baseState from './baseState';

describe(`Dispatching ${START_SUBMIT}`, function() {
    test(`should set 'isSubmitting' to true`, function() {
        const oldState: FormState = { ...baseState, isSubmitting: false };
        const newState = formReducer(oldState, { type: START_SUBMIT });
        expect(newState.isSubmitting).toBe(true);
    });
});

describe(`Dispatching ${END_SUBMIT}`, function() {
    test(`should set 'isSubmitting' to false`, function() {
        const oldState: FormState = { ...baseState, isSubmitting: true };
        const newState = formReducer(oldState, { type: END_SUBMIT });
        expect(newState.isSubmitting).toBe(false);
    });
});
